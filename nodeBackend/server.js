const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const session = require('express-session');
const mongoose = require('mongoose');
const port = process.env.port || 1443;

app.use(session({
    secret: 'sdifhdfbgidbgkdufgkuxdfghkdhgfkjfjkvfgdzvg',
    saveUninitialized: false,
    resave : false
}));

mongoose.Promise = Promise;
mongoose.connect('mongodb://localhost:27017/angTestDB', {useNewUrlParser:true}).then(() => console.log('Mongoose Up..!'));

const User = require('./schemas/users');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//var LoginController = require('./Controllers/LoginController')();

app.use('/api/register', async (req,res) => {
    const {email, password} = req.body;

    const exsistUser = await User.findOne({email});
    if(exsistUser){
        res.json({
            success:false,
            message:"User Already Exsist..!"
        }) 
        return;
    }

    const user = new User({
        email,
        password
    })

    const result = await user.save();
    res.json({
        success:true,
        message:"User Created Succes..!"
    }) 
});

app.use('/api/login', async (req,res) => {
    const {email, password} = req.body;
    console.log(email,password)
    const response = await User.findOne({email,password});
    if(!response){
        res.json({
            success:false,
            message:"Incorrect Credintials..!"
        })
    }else{
        req.session.user = email;
        res.json({
            success:true,
            message:"Success..!"
        })
    }
})

app.get('/api/isloggedin', async (req,res) => {
    await res.json({
        status: !!req.session.user
    })
})

app.get('/api/data', async (req, res) => {

    const user = await User.findOne({email: req.session.user});

    if(!user){
        res.json({
            status:false,
            message:'user was deleted'
        })
        return;
    }

    res.json({
        status:true,
        email:req.session.user,
        quote:user.quote
    })
})

app.get('/api/logout', (req, res) => {
    req.session.destroy();
    res.json({
        success: true
    })
})

app.post('/api/quote', async (req, res) => {
    const user = await User.findOne({email:req.session.user})

    if(!user){
        res.json({
            success:false,
            message:'Invalid User!'
        })
    }

    await User.updateOne({email:req.session.user}, {$set:{quote:req.body.value}})
    res.json({
        success:true
    })
})

app.listen(port,function(){
    var dateime = new Date();
    var message = "Server Running on Port : " + port + " Started at : " + dateime;
    console.log(message);
});