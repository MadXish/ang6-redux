import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

interface myData{
  message:string,
  success:boolean
}

interface registerRes{
  success: boolean;
  message:boolean
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedInStatus = false;

  constructor(private http:HttpClient) { }

  setLoggedIn(value:boolean){
    this.loggedInStatus = value;
  }

  get isLoggedIn(){
    return this.loggedInStatus;
  }

  getUserDetails(email,password){
    return this.http.post<myData>('/api/login',{
      email,password
    })
  }

  registerUser(email, password){
    return this.http.post<registerRes>('/api/register',{
      email,password
    });
  }

}
